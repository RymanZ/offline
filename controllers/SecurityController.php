<?php

namespace app\controllers;

use dektrium\user\controllers\SecurityController as BaseAdminController;

use dektrium\user\Finder;
use dektrium\user\models\LoginForm;
use dektrium\user\Module;
use Yii;
use yii\authclient\AuthAction;
use yii\authclient\ClientInterface;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use dektrium\user\traits\AjaxValidationTrait;

use app\models\User;

class SecurityController extends BaseAdminController
{
  	public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            $this->goHome();
        }

        $model = \Yii::createObject(LoginForm::className());

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
        	User::loginLock();
            
            return $this->goBack();
        }

        return $this->render('login', [
            'model'  => $model,
            'module' => $this->module,
        ]);
    }
}