<?php

namespace app\controllers;

use Yii;
use app\models\Checklog;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\models\CheckForm;
use app\models\ClaimForm;
use app\models\UseForm;

use yii\filters\AccessControl;
use app\filters\AccessRule;
use app\models\User;
use app\models\Validate;
class ValidateController extends Controller
{
    const STATUS_SEND   = 2;
    const STATUS_ACTIVE = 10;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index','claim','use','fail'],
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index','claim','use','fail'],
                        'allow' => true,
                        'roles' => [User::ROLE_ADMIN,User::ROLE_USER],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }

        if(!User::getLoginLock()){
            Yii::$app->user->logout();
            return $this->goHome();
        }

        return true; 
    }

    /**
     * 输入邀请码
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new CheckForm();

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * 校验邀请码
     * @return mixed
     */
    public function actionCheck()
    {
        $request = Yii::$app->request;

        $inviteCode = $request->post('CheckForm')['inviteCode'];
        $model = new Validate();

        //记录日志
        Checklog::addLog('check', $inviteCode);
        
        $response = $model->checkInvite($inviteCode);
        
	    if($response['ret'] == 0){
            $data = $response['data'];
            if(\Yii::$app->user->identity->region != 'SuperAdmin' && \Yii::$app->user->identity->region != $data->region){
                return $this->render('fail', ['inviteCode' => $inviteCode]);
            }

            if($data->status != self::STATUS_ACTIVE && $data->status != self::STATUS_SEND){
                return $this->render('fail', ['inviteCode' => $inviteCode]);
            }
            
	        if($data->status == self::STATUS_SEND && (strtotime($data->send_at) + $data->claim_val * 3600) > time()){
                $model = new ClaimForm();
                return $this->render('claim', [
                    'model' => $model,
                    'inviteCode' => $inviteCode,
                ]);
            }elseif($data->status == self::STATUS_ACTIVE && (strtotime($data->activated_at) + $data->use_val * 3600) > time()){
                $user_id = $data->claimer_id;
                $model = new UseForm();
                return $this->render('use', [
                    'model' => $model,
                    'inviteCode' => $inviteCode,
                    'userID' =>$user_id,
                ]);
            }else{
                return $this->render('fail', ['inviteCode' => $inviteCode]);
            }
        }
        
        if(isset($response['ret'])){
            return $this->render('fail', ['inviteCode' => $inviteCode]);
        }
        //return $this->render('fail', ['inviteCode' => $inviteCode]);
        return $this->render('../site/error',['name' => 'Error', 'message' => 'Service error. Please contact Front of House support if the problem persists.']);
    }

    /**
     * claim邀请码
     * @return mixed
     */
    public function actionClaim()
    {
        $request = Yii::$app->request;

        $email = $request->post('email');
        $inviteCode = $request->post('inviteCode');

        $model = new Validate();

        //校验邮箱和邀请码格式
        if(!$model->isEmail($email) || !$model->isInviteCode($inviteCode)){
            return false;
        }
        
        $result = $model->getUserID($email);
        $user_id = 0;
	    if($result->ret == 1){
            $user_id = $result->data;
        }else{
            return $model->retJson('', 2, 'There is no active OnePlus account associated with this email. Please register at account.oneplus.net first.');
        }

        $response = $model->checkInvite($inviteCode);
        if($response['ret'] == 0){
            $data = $response['data'];
            if(\Yii::$app->user->identity->region != 'SuperAdmin' && \Yii::$app->user->identity->region != $data->region){
                return $this->render('fail', ['inviteCode' => $inviteCode]);
            }
            if($data->status != self::STATUS_SEND || (strtotime($data->send_at) + $data->claim_val * 3600) < time()){
                return $this->render('fail', ['inviteCode' => $inviteCode]);
            }
        }

        $claimResult = $model->claimInvite($inviteCode, $email, $user_id);
        if($claimResult['ret'] == 0){
            //记录日志
            Checklog::addLog('claim', $inviteCode, $email);
            
            return $model->retJson('', 0, 'Successfully claimed.');
        }else{
            return $model->retJson('', 1, 'Failed to claim invite. Please claim the invite manually at invites.oneplus.net.');
        }
    }

    /**
     * Use邀请码
     * @return mixed
     */
    public function actionUse()
    {
        $request = Yii::$app->request;

        $userID = $request->post('userId');
        $inviteCode = $request->post('inviteCode');

        $model = new Validate();

        //校验邀请码格式
        if(!$model->isInviteCode($inviteCode)){
            return false;
        }

        $response = $model->checkInvite($inviteCode);
        if($response['ret'] == 0){
            $data = $response['data'];
            if(\Yii::$app->user->identity->region != 'SuperAdmin' && \Yii::$app->user->identity->region != $data->region){
                return $this->render('fail', ['inviteCode' => $inviteCode]);
            }
            if($data->status != self::STATUS_ACTIVE || (strtotime($data->activated_at) + $data->use_val * 3600) < time()){
                return $this->render('fail', ['inviteCode' => $inviteCode]);
            }
        }

        $useResult = $model->useInvite($inviteCode, $userID);
        if($useResult['ret'] == 0){
            //记录日志
            Checklog::addLog('use', $inviteCode);

            return $model->retJson('', 0, 'Successfully set to used.');
        }else{
            return $model->retJson('', 1, 'Failed to set invite to used. Please try again later.');
        }
    }
}
