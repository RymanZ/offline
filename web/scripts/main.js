'use strict';

(function ($) {

	$('input:visible').eq(0).focus();

	$('.input-invite input').on('keydown',function(e){
		var $this = $(this);
		var input = $this.val();
		if(/^[0-9a-zA-Z]{4}$|^[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}$|^[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}$/.test(input) && e.keyCode !== 8) {
			$this.val(input + '-');
		}
	});

	$('.js-auto-submit').on('click',function(e){
		e.preventDefault();
		e.stopPropagation();
		var $this = $(this);
		var $form = $this.parents('form');
		var $invite = $form.find('.input-invite input');
		if(/[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}/.test($invite.val().trim())) {
			$this.trigger('validated');
		}else{
			$invite.parents('.form-group').addClass('has-error');
		}
	});

	$('.js-auto-submit').on('validated',function(){
		var $this = $(this);
		var $form = $this.parents('form');
		$form.submit();
	});

	$('#useform-doublecheck').on('change',function(){
		$('#use-this-invite').attr('disabled',!this.checked);
	});

	$('#claimform-doublecheck').on('change',function(){
		$('#claim-this-invite').attr('disabled',!this.checked);
	});

	$('#claim-this-invite').on('click',function(e){
		e.preventDefault();
		var param = {
			inviteCode: $('#claimform-invitecode').val(),
			email: $('#claimform-email').val()
		}
		if(!/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(param.email)) {
			return;
		}
		$.post('/validate/claim',param,function(response){
			var $feedbackModal = $('#feedbackModal');
			if(response.ret === 0) {
				$feedbackModal.find('.feedback').text('Successfully set to claimed.');
				$feedbackModal.modal('show');
				$feedbackModal.find('#claim-to-use').show();
			}else {
				$feedbackModal.find('#claim-to-use').hide();
				$feedbackModal.find('.feedback').text(response.errMsg);
				$feedbackModal.modal('show');
			}
		},'json');
	});

	$('#use-this-invite').on('click',function(e){
		e.preventDefault();
		var param = {
			inviteCode: $('#useform-invitecode').val(),
			userId: $('#useform-userid').val(),
		}
		$.post('/validate/use',param,function(response){
			var $feedbackModal = $('#feedbackModal');

			if(response.ret === 0) {
				$feedbackModal.find('.feedback').text('Successfully set to used.');
				$feedbackModal.modal('show');
			}else {
				$feedbackModal.find('.feedback').text(response.errMsg);
				$feedbackModal.modal('show');
			}
		},'json');
	});

})(jQuery);