<?php

use yii\db\Schema;
use yii\db\Migration;

class m150818_023122_create_checklog_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%checklog}}', [
            'id' => Schema::TYPE_PK,
            'customer_email' => Schema::TYPE_STRING.'(255) DEFAULT ""',
            'invite' => Schema::TYPE_STRING . '(20) NOT NULL',
            'action' => Schema::TYPE_STRING . '(20) NOT NULL',
            'user' => Schema::TYPE_STRING . '(25) NOT NULL',
            'user_id' => Schema::TYPE_INTEGER  . ' NOT NULL DEFAULT 0',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%checklog}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
