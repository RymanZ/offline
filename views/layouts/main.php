<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <div class="container">
        <?php
        NavBar::begin([
            'brandLabel' => '',
            'brandUrl' => Yii::$app->homeUrl,
            'options' => [
                'class' => 'navbar-inverse',
            ],
        ]);
        /*echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => [
                ['label' => 'Home', 'url' => ['/site/index']],
                ['label' => 'About', 'url' => ['/site/about']],
                ['label' => 'Contact', 'url' => ['/site/contact']],
                [
                    'label' => 'Status',
                    'items' => [
                        ['label' => 'View', 'url' => ['/status/index']],
                        ['label' => 'Create', 'url' => ['/status/create']],
                    ],
                ],
                Yii::$app->user->isGuest ?
                    ['label' => 'Login', 'url' => ['/site/login']] :
                    [
                        'label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                        'url' => ['/site/logout'],
                        'linkOptions' => ['data-method' => 'post']
                    ],
            ],
        ]);*/
        if (Yii::$app->user->isGuest) {
            $navItems = [];
            array_push($navItems,['label' => 'Sign In', 'url' => ['/user/login']]/*,['label' => 'Sign Up', 'url' => ['/user/register']]*/);
        } else {
            $navItems=[
                ['label' => 'Validate Invites', 'url' => ['/validate/index']]
            ];
            if(Yii::$app->user->identity->isAdmin){
                array_push($navItems,['label' => 'Manage Users',
                'url' => ['/user/admin'],
                //'linkOptions' => ['data-method' => 'post']
                ]);
            }
            /*array_push($navItems,['label' => 'Logout (' . Yii::$app->user->identity->username . ' | '.Yii::$app->user->identity->region.')',
                'url' => ['/site/logout'],
                'linkOptions' => ['data-method' => 'post']]
            );*/
        }
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $navItems,
        ]);
        NavBar::end();
        ?>
    </div>
    

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <?php if(!Yii::$app->user->isGuest){ ?>
            <p class="pull-right"><a href="/site/logout" data-method="post">Logout (<?php echo Yii::$app->user->identity->username . ' | '.Yii::$app->user->identity->region ?>)</a></p>
        <?php } ?>
        <p class="pull-left">&copy; OnePlus <?= date('Y') ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
<?=Html::jsFile('@web/scripts/main.js')?>
</body>
</html>
<?php $this->endPage() ?>
