<?php

/**
 * @var yii\widgets\ActiveForm    $form
 * @var dektrium\user\models\User $user
 */

$regionInputRule = ['maxlength' => 30];

if(\Yii::$app->user->identity->region != 'SuperAdmin'){
	$regionInputRule['value'] = Yii::$app->user->identity->region;
	$regionInputRule['readonly'] = 'readonly';
}
?>

<?= $form->field($user, 'username')->textInput(['maxlength' => 25]) ?>
<?= $form->field($user, 'email')->textInput(['maxlength' => 255]) ?>
<?= $form->field($user, 'password')->passwordInput() ?>
<?= $form->field($user, 'region')->textInput($regionInputRule) ?>
<?= $form->field($user, 'role')->dropDownList([$user::ROLE_USER=>'User',$user::ROLE_ADMIN=>'Admin'], ['prompt'=>'Please Select']) ?>