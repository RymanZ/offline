<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Checklog */

$this->title = 'Create Checklog';
$this->params['breadcrumbs'][] = ['label' => 'Checklogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="checklog-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
