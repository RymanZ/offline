<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\CheckForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Check Invite Code';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-container">
	<div class="margin-top-medium"></div>
	<div class="row">
		<div class="form-horizontal col-xs-12">
	        <div class="form-group input-invite">
	            <input type="text" value="<?=$inviteCode?>" class="form-control" readonly>
	        </div>
	        <p style="margin-top: 10px;">This invite is not valid or has already been used.</p>
	        <div class="form-group">
	            <a class="btn btn-primary" href="/validate/index">Back</a>
	        </div>
	    </div>
	</div>
	<div class="margin-bottom-medium"></div>
</div>
