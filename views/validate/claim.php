<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\CheckForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Check Invite Code';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-container">
    <div class="margin-top-medium"></div>
    <div class="row">
        <?php $form = ActiveForm::begin([
            'action' => '/validate/claim',
            'id' => 'claim-form',
            'options' => ['class' => 'form-horizontal col-xs-12'],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"\">{input}</div>\n<div class=\"\">{error}</div>",
                'labelOptions' => ['class' => 'hide'],
            ],
        ]); ?>
            <?= $form->field($model, 'inviteCode', [
                'template' => "<div class=\"input-invite\">{input}</div>\n<div class=\"\">{error}</div>"
            ])->textInput(['value' => $inviteCode, 'readonly' => 'readonly']) ?>
            <p>This invite is not yet claimed.</p>
            <p class="text-muted">Would the customer like to claim it?</p>
            <?= $form->field($model, 'email', [
                'template' => "<div style=\"margin:0 1em;\">{input}</div>\n<div class=\"\">{error}</div>"
            ])->input('email')->textInput(['placeholder' => 'Enter the customer\'s e-mail']) ?>

            <?= $form->field($model, 'doubleCheck')->checkbox([
                'template' => "<div class=\"double-check\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
            ])->label('Are you sure?') ?>

            <div class="form-group">
                <div class="">
                    <?= Html::submitButton('Claim', ['class' => 'btn btn-primary', 'name' => 'check-button','disabled' => true, 'id' => 'claim-this-invite']) ?>
                </div>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="modal fade" id="feedbackModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
        <form action="/validate/check" method="POST">
            <input type="text" class="hidden" name="CheckForm[inviteCode]" value="<?=$inviteCode?>">
            <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
            <div class="modal-body">
                <h4 class="feedback">Successfully set to claimed.</h4>
            </div>
            <div class="modal-footer" style="text-align: center;">
                <a class="btn btn-muted" href="/validate/index">Back</a>
                <button type="submit" id="claim-to-use" class="btn btn-primary">Use this invite</button>
            </div>
        </form>
        </div>
      </div>
    </div>

</div>
