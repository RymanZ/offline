<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\CheckForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Check Invite Code';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-container">
    <div class="margin-top-medium"></div>
    <div class="row">

        <?php $form = ActiveForm::begin([
            'action' => '/validate/use',
            'id' => 'use-form',
            'options' => ['class' => 'form-horizontal col-xs-12'],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"\">{input}</div>\n<div class=\"\">{error}</div>",
                'labelOptions' => ['class' => 'hide', 'placeholder' => 'Enter Invite'],
            ],
        ]); ?>

            <?= $form->field($model, 'inviteCode',[
                'template' => "<div class=\"input-invite\">{input}</div>\n<div class=\"\">{error}</div>"
            ])->textInput(['value' => $inviteCode, 'readonly' => 'readonly']) ?>
            <p>This invite is valid for purchasing the OnePlus 2</p>
            <?= $form->field($model, 'userID')->textInput(['value' => $userID, 'class' => 'hide']) ?>
            
            <?= $form->field($model, 'doubleCheck')->checkbox([
                'template' => "<div class=\"\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
            ])->label('Are you sure?') ?>

            <div class="form-group">
                <div class="col-xs-12">
                    <?= Html::submitButton('Use', ['class' => 'btn btn-primary', 'name' => 'check-button', 'disabled' => true, 'id' => 'use-this-invite']) ?>
                </div>
            </div>

        <?php ActiveForm::end(); ?>
    </div>
    <div class="margin-bottom-medium"></div>

    <div class="modal fade in" id="feedbackModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <h4 class="feedback">Successfully set to used.</h4>
          </div>
          <div class="modal-footer" style="text-align: center;">
            <a class="btn btn-muted" href="/validate/index">Back</a>
          </div>
        </div>
      </div>
    </div>
    
</div>
