<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\CheckForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Check Invite Code';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-container site-login">
    <div class="row margin-top-medium margin-bottom-medium">
        <?php $form = ActiveForm::begin([
            'action' => '/validate/check',
            'id' => 'check-form',
            'options' => ['class' => 'form-horizontal col-xs-12'],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"input-invite\">{input}</div>\n<div class=\"\">{error}</div>",
                'labelOptions' => ['class' => 'hide', 'placeholder' => 'Enter Invite'],
            ],
        ]); ?>

            <?= $form->field($model, 'inviteCode')->textInput(['maxlength'=>19,'placeholder'=>'Enter Invite']) ?>

            <div class="form-group">
                <div class="">
                    <?= Html::submitButton('Check', ['class' => 'btn btn-primary js-auto-submit', 'name' => 'check-button']) ?>
                </div>
            </div>

        <?php ActiveForm::end(); ?>
    </div>

</div>
