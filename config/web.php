<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views/admin' => '@app/views/admin',
                    '@dektrium/user/views/security' => '@app/views/security'
                ],
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'purcOzHZ67Y8uwT-FnzskQKIfxSvxrBN',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        /*'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],*/
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'viewPath' => '@app/mailer',
            'useFileTransport' => true,
            /*'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.163.com',
                'username' => 'zengchao592618861@163.com',
                'password' => 'zxs753951',
                'port' => '25',
                'encryption' => 'tls',
            ],
            'messageConfig'=>[  
                'charset'=>'UTF-8',  
                'from'=>['zengchao592618861@163.com'=>'zengchao592618861@163.com']  
            ], */
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'modelMap' => [
               'User' => 'app\models\User',
               'UserSearch' => 'app\models\UserSearch',
               'LoginForm' => 'app\models\LoginForm'
            ],
            'controllerMap' => [
                'security' => 'app\controllers\SecurityController'
            ],
            'enableRegistration' => false,    //关闭用户注册
            'enableConfirmation' => false,    //关闭用户认证
            //'enableUnconfirmedLogin' => true, //未认证用户也可以登录
            'enablePasswordRecovery' => false,//找回密码找管理员
            'confirmWithin' => 28800,
            'cost' => 12,
            //'admins' => ['admin','test'],
            /*'mailer' => [
                'sender'                => 'zengchao592618861@163.com', // or ['no-reply@myhost.com' => 'Sender name']
                'welcomeSubject'        => 'Welcome subject',
                'confirmationSubject'   => 'Confirmation subject',
                'reconfirmationSubject' => 'Email change subject',
                'recoverySubject'       => 'Recovery subject',
            ],*/
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
