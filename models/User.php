<?php
namespace app\models;

use dektrium\user\models\User as BaseUser;
use Yii;

class User extends BaseUser
{
	const ROLE_USER = 10;
    const ROLE_ADMIN = 100;

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        // add field to scenarios
        $scenarios['create'][]   = 'role';
        $scenarios['update'][]   = 'role';
        $scenarios['register'][] = 'role';
        $scenarios['create'][]   = 'region';
        $scenarios['update'][]   = 'region';
        $scenarios['register'][] = 'region';

        /*$scenarios['create'][]   = 'status';
        $scenarios['update'][]   = 'status';
        $scenarios['settings'][]   = 'status';*/
        return $scenarios;
    }

    public function rules()
    {
        $rules = parent::rules();
        // add some rules
        $rules['roleRequired'] = ['role', 'required'];
        $rules['regionRequired'] = ['region', 'required'];
        $rules['regionLength']   = ['region', 'string', 'max' => 30];
        
        return $rules;
    }

    public function getIsAdmin()
    {
        return $this->role == self::ROLE_ADMIN;
    }

    public function attributeLabels()
    {
    	$attributes = parent::attributeLabels();
    	$attributes['region'] = \Yii::t('user', 'Region');
    	$attributes['role'] = \Yii::t('user', 'Role');
        
        return $attributes;
    }

    public static function loginLock()
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }
        $user = self::find()->where(['id' => Yii::$app->user->identity->ID])->one();
        return (bool) $user->updateAttributes(['status' => Yii::$app->session->getId()]);
    }

    public static function getLoginLock(){
        if (Yii::$app->user->isGuest) {
            return false;
        }
        $user = self::find()->where(['id' => Yii::$app->user->identity->ID])->one();
        return $user->status == Yii::$app->session->getId();
    }
}