<?php

namespace app\models;

use Yii;
use yii\base\Model;

class ClaimForm extends Model
{
    public $inviteCode;

    public $email;

    public $doubleCheck;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['inviteCode','email'], 'required'],
            ['doubleCheck', 'boolean'],
        ];
    }
}
