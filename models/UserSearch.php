<?php

namespace app\models;

use Yii;
use dektrium\user\models\UserSearch as BaseUserSearch;
use dektrium\user\Finder;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class UserSearch extends BaseUserSearch
{
    /** @var string */
    public $region;

    /** @var string */
    public $role;

    /** @inheritdoc */
    public function rules()
    {
        return [
            'fieldsSafe' => [['username', 'email', 'region', 'role', 'created_at'], 'safe'],
            'createdDefault' => ['created_at', 'default', 'value' => null]
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        $attributes = parent::attributeLabels();
    	$attributes['region'] = \Yii::t('user', 'Region');
    	$attributes['role'] = \Yii::t('user', 'Role');
        
        return $attributes;
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this->finder->getUserQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if(Yii::$app->user->identity->region != 'SuperAdmin'){
            $query->andFilterWhere(['region' => Yii::$app->user->identity->region]);
        }

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }
        
        if ($this->created_at !== null) {
            $date = strtotime($this->created_at);
            $query->andFilterWhere(['between', 'created_at', $date, $date + 3600 * 24]);
        }

        /*if($this->role !== null){
        	if(stripos('admin', $this->role) !== false){
        		$this->role = User::ROLE_ADMIN;
        	}elseif(stripos('user', $this->role) !== false){
        		$this->role = User::ROLE_USER;
        	}
        }*/
        
        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like','region', $this->region])
            ->andFilterWhere(['role' => $this->role]);

        return $dataProvider;
    }
}
