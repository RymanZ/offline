<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "checklog".
 *
 * @property integer $id
 * @property string $customer_email
 * @property string $invite
 * @property string $action
 * @property string $user
 * @property integer $user_id
 * @property integer $created_at
 */
class Checklog extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'checklog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invite', 'action', 'user', 'user_id', 'created_at'], 'required'],
            [['user_id', 'created_at'], 'integer'],
            [['customer_email'], 'string', 'max' => 255],
            [['invite', 'action'], 'string', 'max' => 20],
            [['user'], 'string', 'max' => 25]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_email' => 'Customer Email',
            'invite' => 'Invite Code',
            'action' => 'Action',
            'user' => 'User',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
        ];
    }

    public static function addLog($action, $inviteCode, $email=NULL){
        $model = new self();
        $model->user = Yii::$app->user->identity->username;
        $model->user_id = Yii::$app->user->identity->ID;

        $model->created_at = time();

        $model->action = $action;
        $model->invite = $inviteCode;
        $model->customer_email = empty($email) ? $email : '';

        $model->save();
    }
}
