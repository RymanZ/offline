<?php

namespace app\models;

use dektrium\user\models\LoginForm as BaseLoginForm;
use app\controllers;
class LoginForm extends BaseLoginForm
{
    /**
     * @var string
     */
    public $captcha;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['captcha', 'required'];
        $rules[] = ['captcha', 'captcha', 'captchaAction'=>'/site/captcha'];
        return $rules;
    }
    /*public function rules()
    {
    	$rules = parent::rules();

    	$rules['onceLoginValidate'] = ['login', function ($attribute) {
                if ($this->user !== null) {
                    if($this->user->getLoginLock()){
                    	$this->addError($attribute, \Yii::t('user', 'You can only login your account in one brower.'));
                    }
                }
            }];

        return $rules;
    }*/
}