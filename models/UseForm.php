<?php

namespace app\models;

use Yii;
use yii\base\Model;

class UseForm extends Model
{
    public $inviteCode;

    public $doubleCheck;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['inviteCode'], 'required'],
            ['doubleCheck', 'boolean'],
        ];
    }
}
