<?php

namespace app\models;

use Yii;
use yii\base\Object;
use app\models\Checklog;

/**
 * Class Validate
 * @package app\models
 */
class Validate extends Object
{
    private $site = 'offline';

    private $secret = 'b8e0km5fdsffr6es3cf1f85b0dd7sff3';

    private $apiUrl = 'https://api.sandbox.oneplus.net/';

    private $accountUrl = 'https://account.oneplus.net/api/user/getUserByEmail';

    private $accountApiKey = 'e6baf6310b46d961d68a4a5063f92422';

    /**
     * 校验邀请码状态
     * @param $inviteCode
     * @return 
     *
     */
    public function checkInvite($inviteCode){
        $apiFunc = $this->apiUrl.'index.php?r=apis/view';

        $data['key'] = $this->site;
        $data['code'] = $inviteCode;
        $data['sign'] = $this->makeSign($data, $this->secret);

        $apiFunc .= '&key='.$data['key'].'&code='.$data['code'].'&sign='.$data['sign'];

        $result = $this->curlGet($apiFunc);

        return $result;
    }

    /**
     * 认领邀请码
     * @param $inviteCode
     * @param $email
     * @param $user_id
     * @return 
     *
     */
    public function claimInvite($inviteCode, $email, $user_id){
        $apiFunc = $this->apiUrl.'index.php?r=apis/claim';

        $data['key'] = $this->site;
        $data['code'] = $inviteCode;
        $data['user_id'] = $user_id;
        $data['email'] = $email;
        $data['sign'] = $this->makeSign($data, $this->secret);

        $apiFunc .= '&key='.$data['key'].'&code='.$data['code'].'&email='.$data['email'].'&user_id='.$data['user_id'].'&sign='.$data['sign'];
        $result = $this->curlGet($apiFunc);

        return $result;
    }

    /**
     * 根据邮箱获取用户id
     * @param $email
     * @return int $user_id
     *
     */
    public function getUserID($email){
        $timestamp = time();
        $strToSign = implode('_', [md5(http_build_query(['email' => $email])), $this->accountApiKey, '__oneplus__', $timestamp]);
        $sign = md5($strToSign);

        $data = ['message' => [
                    'apikey' => $this->accountApiKey, 
                    'arguments' => [
                        'email' => $email
                    ],
                    'timestamp' => $timestamp,
                    'sign' => $sign
                ]
            ];

        return $this->curlJson($this->accountUrl, json_encode($data));
    }

    /**
     * 使用邀请码
     * @param $inviteCode
     * @param $user_id
     * @return 
     *
     */
    public function useInvite($inviteCode, $user_id){
        $apiFunc = $this->apiUrl.'index.php?r=apis/used';

        $data['key'] = $this->site;
        $data['code'] = $inviteCode;
        $data['user_id'] = $user_id;
        $data['sign'] = $this->makeSign($data, $this->secret);

        $apiFunc .= '&key='.$data['key'].'&code='.$data['code'].'&user_id='.$data['user_id'].'&sign='.$data['sign'];
        $result = $this->curlGet($apiFunc);

        return $result;
    }

    /**
     * 检测邮箱是否合法
     * @param $email
     * @return int
     */
    public static function isEmail($email) {
        $result = 0;
        if (preg_match("/^\w+([\+\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/i",$email)) {
            $result = 1;
        }

        return $result = 1;
    }

    /**
     * 检测邀请码是否合法
     * @param $inviteCode
     * @return int
     */
    public static function isInviteCode($inviteCode) {
        $result = 0;
        if (1) {
            $result = 1;
        }

        return $result;
    }

    /**
     * 数据JSON包装输出
     * @param null $data
     * @param int $code
     * @param null $errMsg
     * @param null $page
     */
    public static function retJson($data = null, $code = 0, $errMsg = null, $page = null)
    {

        if(NULL == $page){
            $result = ['ret' => $code, 'errMsg' => $errMsg, 'data' => $data];
        }else{
            $result = ['ret' => $code, 'errMsg' => $errMsg, 'data' => $data, 'paging' =>$page];
        }

        echo json_encode($result);
        exit;
    }

    /**
     * 数据JSONP包装输出
     * @param null $data
     * @param int $code
     * @param null $errMsg
     * @param null $page
     */
    public static function retJsonp($data = null, $code = 0, $errMsg = null, $page = null){
        if(NULL == $page){
            $result = ['ret' => $code, 'errMsg' => $errMsg, 'data' => $data];
        }else{
            $result = ['ret' => $code, 'errMsg' => $errMsg, 'data' => $data, 'paging' =>$page];
        }

        $strJson  = json_encode($result);
        $callBack = \Yii::$app->request->get('success_jsonpCallback');
        $callBack = Html::encode($callBack);
        if ($callBack) {
            $strJson = $callBack . '('. $strJson . ')';
        }
        echo $strJson; exit;
    }

    /**
     * 发送POST请求
     * @param string $url
     * @param string $json
     * @return json
     */
    public function curlJson($url, $json){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($json)
        ]);
        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response);
    }

    /**
     * 发送POST请求
     * @param $url
     * @param $data
     * @return mixed
     */
    public function curlArray($url, $data){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_URL, $url);
        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response);
    }

    /**
     * [_postCurlGet CURL get请求]
     * @param  [type] $url      [请求url]
     * @return [type]           [description]
     */
    private function curlGet($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);//https请求不验证证书
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);//https请求不验证hosts
        $returnData = curl_exec($ch);
        curl_close($ch);
        $returnData = (array)json_decode($returnData);
        return $returnData;
    }

    /**
     * 生成加密验证串
     * @param $input
     * @param $secret
     * @return null|string
     */
    public function makeSign($input, $secret){
        ksort($input);
        $inputStr = null;

        if(is_array($input)){
            foreach($input as $key => $value){
                $inputStr .= $key.$value;
            }
        }
        $inputStr = md5(strtolower($inputStr).strtolower($secret));
        return $inputStr;
    }

}
